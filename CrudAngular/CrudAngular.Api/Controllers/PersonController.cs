﻿using System.Threading.Tasks;
using CrudAngular.Api.ApplicationContract.IServices;
using CrudAngular.Api.ApplicationContract.Services.Persons.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace CrudAngular.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonAppService _personAppService;
        public PersonController(IPersonAppService personAppService)
        {
            this._personAppService = personAppService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _personAppService.GetAllAsync();

            return Ok(result);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            PersonDto result = await _personAppService.GetAsync(id);

            return Ok(result);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] PersonDto person)
        {
            long personId = await _personAppService.InsertAndGetIdAsync(person);
            person.Id = personId;

            return Ok(person);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] PersonDto personInput)
        {
            PersonDto person = await _personAppService.UpdateAsync(id, personInput);
            return Ok(person);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _personAppService.Delete(id);

            return Ok();
        }

    }
}