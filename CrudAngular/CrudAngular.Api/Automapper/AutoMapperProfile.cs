﻿using AutoMapper;
using CrudAngular.Api.ApplicationContract.Services.Persons.Dtos;
using CrudAngular.Api.CoreContract.Models;

namespace CrudAngular.Api.Automapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Person, PersonDto>().ReverseMap();
        }
    }
}
