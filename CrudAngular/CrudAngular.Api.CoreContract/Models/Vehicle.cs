﻿using CrudAngular.Api.CoreContract.Db;

namespace CrudAngular.Api.CoreContract.Models
{
    public class Vehicle : IEntity<int>
    {
        public string Brand { get; set; }

        public virtual Person Person { get; set; }
    }
}
