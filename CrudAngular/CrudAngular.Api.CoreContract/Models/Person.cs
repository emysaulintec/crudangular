﻿using CrudAngular.Api.CoreContract.Db;
using System.Collections.Generic;

namespace CrudAngular.Api.CoreContract.Models
{
    public class Person : IEntity<long>
    {
        public Person()
        {
            Vehicles = new HashSet<Vehicle>();
        }

        public string Name { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}
