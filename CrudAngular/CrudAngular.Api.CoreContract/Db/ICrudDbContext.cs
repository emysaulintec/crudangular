﻿using CrudAngular.Api.CoreContract.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace CrudAngular.Api.CoreContract.Db
{
    public interface ICrudDbContext
    {
        DbSet<Person> People { get; set; }
        DbSet<Vehicle> Vehicles { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
