﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudAngular.Api.CoreContract.Db
{
    public class IEntity<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }

        public DateTime CreationTime { get; private set; } = DateTime.Now;
        public DateTime? Modificationtime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
