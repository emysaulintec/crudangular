﻿using CrudAngular.Api.CoreContract.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrudAngular.Api.CoreContract.EntityConfigs
{
    public class PersonEntityConfig
    {
        private const int MAX_NAME_LENGTH = 200;
        private const int MAX_USER_NAME_LENGTH = 50;

        public static void SetEntityBuilder(EntityTypeBuilder<Person> entityBuilder)
        {
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.Property(p => p.Id).IsRequired();

            entityBuilder.Property(p => p.Name).HasMaxLength(MAX_NAME_LENGTH);
            entityBuilder.Property(p => p.UserName).HasMaxLength(MAX_USER_NAME_LENGTH);

            entityBuilder.HasIndex(p => p.UserName).IsUnique();
        }
    }
}
