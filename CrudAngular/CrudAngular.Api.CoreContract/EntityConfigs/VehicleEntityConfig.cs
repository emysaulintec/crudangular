﻿using CrudAngular.Api.CoreContract.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrudAngular.Api.CoreContract.EntityConfigs
{
    public class VehicleEntityConfig
    {
        private const int MAX_BRAND_LENGTH = 200;

        public static void SetEntityBuilder(EntityTypeBuilder<Vehicle> entityBuilder)
        {
            entityBuilder.HasKey(p => p.Id);
            entityBuilder.Property(p => p.Id).IsRequired();
            entityBuilder.Property(p => p.Brand).HasMaxLength(MAX_BRAND_LENGTH);
            entityBuilder.HasIndex(p => p.Brand).IsUnique();


            entityBuilder.HasOne(p => p.Person).WithMany(p => p.Vehicles);
        }
    }
}
