﻿using CrudAngular.Api.Application.Services;
using CrudAngular.Api.Application.Services.Persons;
using CrudAngular.Api.ApplicationContract.IServices;
using CrudAngular.Api.Core;
using CrudAngular.Api.CoreContract.Db;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CrudAngular.Api.Common.Register
{
    public static class IoCRegister
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddRegisterBase(services);
            AddRegisterServices(services);

            return services;
        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {
            IEnumerable<Type> baseInterfaces = new Type[] { typeof(IBaseRepository<,>), typeof(IBaseAppService<,,>), typeof(IApplicationService) };

            var serviceList = typeof(ApplicationService).Assembly.GetTypes();
            var interfaceList = typeof(IApplicationService).Assembly.GetTypes().Where(type => type.IsInterface && !baseInterfaces.Contains(type));

            foreach (var currentInterface in interfaceList)
            {
                var basedType = serviceList.FirstOrDefault(d => d.GetInterfaces().Any(e => e == currentInterface));

                if (basedType != null)
                    services.AddScoped(currentInterface, basedType);
            }

            return services;
        }

        private static IServiceCollection AddRegisterBase(IServiceCollection services)
        {
            services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));

            return services;
        }
    }
}
