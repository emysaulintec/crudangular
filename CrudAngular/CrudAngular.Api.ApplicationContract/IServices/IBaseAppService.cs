﻿using CrudAngular.Api.CoreContract.Db;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrudAngular.Api.ApplicationContract.IServices
{
    public interface IBaseAppService<TEntity, TDto, TPrimaryKey> : IApplicationService where TEntity : IEntity<TPrimaryKey> where TDto : class
    {
        Task<IEnumerable<TDto>> GetAllAsync();
        Task Delete(TPrimaryKey id);
        Task<TDto> GetAsync(TPrimaryKey id);
        Task<TPrimaryKey> InsertAndGetIdAsync(TDto entity);
        Task<TDto> UpdateAsync(TPrimaryKey id, TDto dto);
    }
}
