﻿using CrudAngular.Api.ApplicationContract.Services.Persons.Dtos;
using CrudAngular.Api.CoreContract.Models;

namespace CrudAngular.Api.ApplicationContract.IServices
{
    public interface IPersonAppService : IBaseAppService<Person, PersonDto, long>
    {
    }
}
