﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudAngular.Api.ApplicationContract.Services.Persons.Dtos
{
    public class PersonDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
}
