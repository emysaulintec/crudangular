﻿using AutoMapper;
using CrudAngular.Api.ApplicationContract.IServices;
using CrudAngular.Api.CoreContract.Db;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrudAngular.Api.Application.Services
{
    public abstract class BaseAppService<TEntity, TDto, TPrimaryKey> : ApplicationService, IBaseAppService<TEntity, TDto, TPrimaryKey> where TEntity : IEntity<TPrimaryKey> where TDto : class
    {
        private readonly IBaseRepository<TEntity, TPrimaryKey> _baseRepository;
        private readonly IMapper _mapper;

        public BaseAppService(IBaseRepository<TEntity, TPrimaryKey> personRepository, IMapper mapper)
        {
            this._baseRepository = personRepository;
            this._mapper = mapper;
        }

        public async Task<IEnumerable<TDto>> GetAllAsync()
        {
            IEnumerable<TEntity> entities = await _baseRepository.GetListAsync();
            IEnumerable<TDto> mappedEntities = _mapper.Map<IEnumerable<TDto>>(entities);

            return mappedEntities;
        }

        public async Task Delete(TPrimaryKey id)
        {
            await _baseRepository.DeleteAsync(id);
        }

        public async Task<TDto> GetAsync(TPrimaryKey id)
        {
            var entity = await _baseRepository.GetAsync(id);

            return _mapper.Map<TDto>(entity);
        }
        public async Task<TPrimaryKey> InsertAndGetIdAsync(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);

            return await _baseRepository.InsertAndGetIdAsync(entity);
        }

        public async Task<TDto> UpdateAsync(TPrimaryKey id, TDto dto)
        {
            var entity = await _baseRepository.GetAsync(id);

            SetDataUpdate(entity, dto);

            var result = await _baseRepository.UpdateAsync(entity);

            return _mapper.Map<TDto>(result);
        }

        public abstract void SetDataUpdate(TEntity entity, TDto dto);

    }
}
