﻿using AutoMapper;
using CrudAngular.Api.ApplicationContract.IServices;
using CrudAngular.Api.ApplicationContract.Services.Persons.Dtos;
using CrudAngular.Api.CoreContract.Db;
using CrudAngular.Api.CoreContract.Models;

namespace CrudAngular.Api.Application.Services.Persons
{
    public class PersonAppService : BaseAppService<Person, PersonDto, long>, IPersonAppService
    {
        public PersonAppService(IBaseRepository<Person, long> personRepository, IMapper mapper) : base(personRepository, mapper)
        {
        }

        public override void SetDataUpdate(Person entity, PersonDto dto)
        {
            entity.Name = dto.Name;
            entity.UserName = dto.UserName;
        }
    }
}
