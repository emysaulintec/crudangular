﻿using CrudAngular.Api.CoreContract.Db;
using CrudAngular.Api.CoreContract.EntityConfigs;
using CrudAngular.Api.CoreContract.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudAngular.Api.EntityFramework.Db
{
    public class CrudDbContext : DbContext, ICrudDbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public CrudDbContext(DbContextOptions options) : base(options) { }
        public CrudDbContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            PersonEntityConfig.SetEntityBuilder(modelBuilder.Entity<Person>());
            VehicleEntityConfig.SetEntityBuilder(modelBuilder.Entity<Vehicle>());

            base.OnModelCreating(modelBuilder);
        }
    }
}
