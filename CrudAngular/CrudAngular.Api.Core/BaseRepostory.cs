﻿using CrudAngular.Api.CoreContract.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrudAngular.Api.Core
{
    public class BaseRepository<TEntity, TPrimaryKey> : IBaseRepository<TEntity, TPrimaryKey> where TEntity : IEntity<TPrimaryKey>
    {
        private readonly ICrudDbContext _crudDbContext;

        public BaseRepository(ICrudDbContext crudDbContext)
        {
            this._crudDbContext = crudDbContext;
        }

        public int Count()
        {
            return GetAll().Count();
        }
        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().Count(predicate);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetAll().FirstOrDefaultAsync(predicate);
        }
        public async Task<TEntity> LastOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetAll().LastOrDefaultAsync(predicate);
        }

        public async Task<TEntity> GetAsync(TPrimaryKey id)
        {
            var result = await _crudDbContext.Set<TEntity>().FindAsync(id);

            if (result != null && result.IsDeleted)
                return null;

            return result;
        }

        public async Task<IEnumerable<TEntity>> GetListAsync()
        {
            return await GetAll().ToListAsync();
        }
        public async Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetAll(predicate).ToListAsync();
        }
        public IQueryable<TEntity> GetAll()
        {
            return _crudDbContext.Set<TEntity>().Where(e => !e.IsDeleted);
        }
        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }
        public IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] propertySelectors)
        {
            var entities = GetAll();

            foreach (var propertyToInclude in propertySelectors)
                entities.Include(propertyToInclude);

            return entities;
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            await _crudDbContext.Set<TEntity>().AddAsync(entity);
            await _crudDbContext.SaveChangesAsync();

            return entity;
        }
        public async Task<TPrimaryKey> InsertAndGetIdAsync(TEntity entity)
        {
            await _crudDbContext.Set<TEntity>().AddAsync(entity);
            await _crudDbContext.SaveChangesAsync();

            return entity.Id;
        }
        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            entity.Modificationtime = DateTime.Now;

            _crudDbContext.Set<TEntity>().Update(entity);
            await _crudDbContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(TEntity entity)
        {
            entity.IsDeleted = true;

            if (entity != null)
            {
                _crudDbContext.Set<TEntity>().Update(entity);
                await _crudDbContext.SaveChangesAsync();
            }
            else
                throw new Exception("Error. Entidad no encontrada.");

        }
        public async Task DeleteAsync(TPrimaryKey id)
        {
            TEntity entity = await _crudDbContext.Set<TEntity>().FindAsync(id);
            await DeleteAsync(entity);
        }
        public async Task DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            TEntity entity = await _crudDbContext.Set<TEntity>().FirstOrDefaultAsync(predicate);

            await DeleteAsync(entity);

        }
    }
}
